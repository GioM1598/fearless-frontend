import Nav from './Nav'
import AttendeesList from './AttendeesList'

function App(props) {
  if (props.attendees === undefined){
    return null;
  }
  console.log("string", props.attendees)
  return (
  <>
    <Nav />
    <div className="container">
      <AttendeesList attendees = {props.attendees} />
    </div>
  </>
  );
}

export default App;
